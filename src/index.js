const express = require('express');
const cors = require('cors');
const UUID = require('uuid');
const uuid = UUID.v4;

const app = express();

app.use(cors());
app.use(express.json());

let users = [];

function findUserOnList(username) {
  return users.find(d => d.username === username);
}

function updateUserOnList(user) {
  users = users.map(d => d.username === user.username ? user : d);
  return user;
}

function addTodo(username, newTodo) {
  const user = findUserOnList(username);
  user.todos = [...user.todos, newTodo];
  updateUserOnList(user);

  return newTodo;
}

function updateTodo(username, todo) {
  const user = findUserOnList(username);

  user.todos = user.todos.map(d => (
    d.id === todo.id
      ? { 
        ...d, 
        ...todo.title && { title: todo.title },
        ...todo.deadline && { deadline: todo.deadline },
        ...todo.done && { done: todo.done },
      }
      : d
    ));

  updateUserOnList(user);

  return user.todos.find(d => d.id === todo.id);
}

function removeTodo(username, todoId) {
  const user = findUserOnList(username);
  user.todos = user.todos.filter(d => d.id !== todoId);
  updateUserOnList(user);

  return user.todos;
}

function checksExistsUserAccount(request, response, next) {
  const { username } = request.headers;
  const userDoesExist = users.some(d => d.username === username);

  if (!userDoesExist) {
    return response.status(404)
      .json({ code: 404, error: 'User does not exist' });
  }

  return next();
}

function checksExistsUserAccountOnCreate(request, response, next) {
  const { username } = request.body;
  const userDoesExist = users.some(d => d.username === username);

  if (userDoesExist) {
    return response.status(400)
      .json({ code: 400, error: 'User already exists' });
  }

  return next();
}

function checkIfTodoExists(request, response, next) {
  const { username } = request.headers;
  const { id } = request.params;
  const user = findUserOnList(username);
  const todoDoesExist = user.todos.some(d => d.id === id);

  if(!todoDoesExist) {
    return response.status(404)
      .json({ code: 404, error: 'Todo does not exist' });
  }

  return next();
}

app.post('/users', checksExistsUserAccountOnCreate, (request, response) => {
  const { name, username } = request.body;

  const newUser = {
    id: uuid(),
    name,
    username,
    todos: [],
  };

  users = [...users, newUser];

  return response.status(201).json(newUser);
});

app.get('/todos', checksExistsUserAccount, (request, response) => {
  const { username } = request.headers;
  const user = findUserOnList(username);
  return response.status(200).json(user.todos);
});

app.post('/todos', checksExistsUserAccount, (request, response) => {
  const { username } = request.headers;
  const { title, deadline } = request.body;

  const newTodo = {
    id: uuid(),
    title,
    deadline: new Date(deadline),
    created_at: new Date(),
    done: false,
  }

  const todo = addTodo(username, newTodo);

  return response.status(201).json(todo);
});

app.put('/todos/:id', 
  checksExistsUserAccount, 
  checkIfTodoExists,
  (request, response) => {
    const { username } = request.headers;
    const { id } = request.params;
    const { title, deadline } = request.body;

    const newTodo = {
      id,
      title,
      deadline: new Date(deadline),
    };

    const todo = updateTodo(username, newTodo)

    return response.status(200).json(todo);
  });

app.patch('/todos/:id/done',
  checksExistsUserAccount,
  checkIfTodoExists,
  (request, response) => {
    const { username } = request.headers;
    const { id } = request.params;

    const newTodo = {
      id,
      done: true,
    };

    const todo = updateTodo(username, newTodo)

    return response.status(200).json(todo);
  });

app.delete('/todos/:id',
  checksExistsUserAccount,
  checkIfTodoExists,
  (request, response) => {
    const { username } = request.headers;
    const { id } = request.params;

    removeTodo(username, id);

    return response.status(204).send();
  });

module.exports = app;